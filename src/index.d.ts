import { Common } from './ingenico-mpos.common';
export declare class IngenicoMpos extends Common {
    private ingenico;
    discoveredDevices: any[];
    selectedDevice: any;
    deviceStatusHandler: any;
    pairedAndSetup: boolean;
    initialize(apiKey: string, url: string, clientVersion: string): Promise<null>;
    login(userName: string, password: string): Promise<null>;
    listenForDevice(): Promise<null>;
    findReader(confirmationCallback: ConfirmationHandler): Promise<null>;
    setupDevice(): Promise<null>;
    isWiredHeadsetOn(): boolean;
    processCreditSale(saleRequest: SaleRequest): Promise<any>;
    processDebitCardSale(saleRequest: SaleRequest): Promise<any>;
    signaturRequired(transactionResponse: any): boolean;
    getTransactionForSignature(): Promise<string>;
    uploadSignature(transactionId: string, image: string): Promise<null>;
    getTransactionHistory(query: any) : Promise<any>;
    voidTransaction(transactionId: string, clerkId: string, gpsLongitude: string, gpsLatitude: string, updateCB: ICardCallback): Promise<any>;
    refundTransaction(transactionId: string, amount: Amount, clerkId: string, gpsLongitude: string, gpsLatitude: string, updateCB: ICardCallback): Promise<any>;
}
export interface ConfirmationHandler {
    (readerPasskey: string, mobilePasskey: string): Promise<boolean>;
}
export interface SaleRequest {
    amount: Amount;
    products: Product[];
    gpsLongitude: string;
    gpsLatitude: string;
    transactionGroupID: string;
}
export interface Amount {
    currency: string;
    total: number;
    subtotal: number;
    tax: number;
    discount: number;
    discountDescription: string;
    tip: number;
}
export interface Product {
    name: string;
    price: number;
    description: string;
    image: string;
    quantity: number;
}

export interface ICardCallback {
    (updateCode: number, extraMessage: string): void;
}

export interface CreditSaleResponse {
    responseCode: any,
    transactionResponse: any
}

export interface TransactionHistoryResponse {
    responseCode: any,
    transactions: any[]
}

export interface TransactionHistorySummary {
    transactionType : string;
    transactionId : string;
    clerkId : string;
    amount : number;
    approvedAmount : number;
    deviceTimestamp : string;
    deviceTimezone: string;
    status: string;
    responseCode: string;
}