import { Observable} from 'tns-core-modules/data/observable';
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { IngenicoMpos, SaleRequest, Product, Amount } from 'nativescript-ingenico-mpos';
import { DrawingPad } from 'nativescript-drawingpad';
import { isEnabled, enableLocationRequest, getCurrentLocation, watchLocation, distance, clearWatch } from 'nativescript-geolocation';
import { topmost } from 'ui/frame';
import * as permissions from 'nativescript-permissions';
import * as dialogs from 'tns-core-modules/ui/dialogs';
import * as platformModule from "tns-core-modules/platform";
import { CreditSaleResponse, TransactionHistoryResponse, TransactionHistorySummary } from '../../src/ingenico-mpos.android';

declare var android;

export class HelloWorldModel extends Observable {

	public message: string;
	public log : string[] = [];
	private ingenicoMpos: IngenicoMpos;
	private testList: TransactionHistorySummary[] = [{
        "responseCode": "Approved",
        "transactionId": "48736a",
        "clerkId": "001",
        "amount": 100,
        "approvedAmount": 100,
        "transactionType": null,
        "deviceTimestamp": "20170926172438",
        "deviceTimezone": null,
        "status": "Completed"
    }, {
        "responseCode": "Declined",
        "transactionId": "48643",
        "clerkId": "001",
        "amount": 50036,
        "approvedAmount": 0,
        "transactionType": null,
        "deviceTimestamp": "20170925205939",
        "deviceTimezone": null,
        "status": "Unknown"
    }, {
        "responseCode": "Declined",
        "transactionId": "48642",
        "clerkId": "001",
        "amount": 50033,
        "approvedAmount": 0,
        "transactionType": null,
        "deviceTimestamp": "20170925205650",
        "deviceTimezone": null,
        "status": "Unknown"
    }
    ];
	private API_KEY = 'RPX6-d76d2c37-f7b7-40c8-bf3f-9ec0a5cdc562';
	private DEFAULT_BASE_URL = 'https://uatmcm.roamdata.com/';
	private CLIENT_VERSION = '0.1';
	private DEMO_LONGITUDE = '-122.084';
    private DEMO_LATITUDE = '37.422';

	private readerReady: boolean = false;

    public screenHeight:number = platformModule.screen.mainScreen.heightPixels;
    public screenWidth:number = platformModule.screen.mainScreen.widthPixels;
	public signaturePadShowing:boolean = false;
	public transactionStarted: boolean = false;
    public saleRequest: SaleRequest;

    public dialogStack: any[] = [];
    public cancelTransactionId: string = '48394';
    public cancelClerkId: string = '001';
    public cancelAmount: number = 100;

    public padHeight:number = 200;
    public currentTransactionId:string = null;
    public product: Product  = {
        description: 'Test Product',
        name: 'Test Product',
        price: 1.00,
        quantity: 1,
        image: null
    };

    public amount: Amount = {
        currency: 'USD',
        discount: 0,
        discountDescription: '',
        subtotal: 50,
        tax: 0.00,
        tip: 0.00,
        total: 1000.50
    };
    // public transactionHistory: Map<string, any> = new Map<string, any>();
    public transactionHistory: ObservableArray<TransactionHistorySummary> = new ObservableArray(this.testList);

	constructor() {
		super();
		this.message = 'Testing';
		this.logMessage('Starting Up..');
		this.getPermissions()
            .then(() => {

        this.readerReady = false;
        super.notifyPropertyChange('readerReady', false, true);
		this.ingenicoMpos = new IngenicoMpos();
		this.ingenicoMpos.initialize(this.API_KEY, this.DEFAULT_BASE_URL, this.CLIENT_VERSION)
			.then(() => {
				this.logMessage('Intialized');
				this.ingenicoMpos.login('azovatest1', '12345')
					.then(() => {
						this.logMessage('Login Successful');
                        this.logMessage('trying to load transactons');
						this.ingenicoMpos.listenForDevice()
							.then(() => {
								this.ingenicoMpos.findReader((readerText:string, deviceText:string):Promise<boolean> => {
									return new Promise((resolve, reject) => {
										dialogs.confirm({
											title: 'Confirm Pairing',
											message: `Do these match? Reader: ${readerText} Device: ${deviceText}`,
											okButtonText: 'Yes',
											cancelButtonText: 'No',
											neutralButtonText: null
										}).then(result => {
											this.logMessage('Reader matchces..');
											resolve(result);
										}).catch(err => {
											reject(err);
										})
									});
								})
								.then(() => {
								    this.logMessage('Reader found. trying to setupDeviceF');
									this.ingenicoMpos.setupDevice().then(() => {
										this.logMessage('ready to use reader???!!');
										this.readerReady = true;
                                        super.notifyPropertyChange('readerReady', true, false);

										//alert('ready to use reader????!!!!!');
									})
									.catch(err => {
										console.log('Could not setup device Error:', err);
										this.logMessage('could not setup device');
										alert('could not setup device');
                                        this.readerReady = false;
                                        super.notifyPropertyChange('readerReady', false, true);
									})
								})
								.catch(err => {
									alert(err);
								});
							})
							.catch(err => {
								alert('listenForDevice:'+ err);
							});
					})
					.catch(errCode => {
						alert('Login Error Code: ' + errCode);
					});
			})
			.catch(errCode => {
				alert('Error Code: ' + errCode);
			});
            })
            .catch((msg:any) => {
		        alert('Permissions are required.' + msg);
            });
	}

	loadTransactions() : Promise<null> {
	    let query:any = {};
	    return new Promise((resolve, reject) => {
            this.logMessage('before making call to load transactions');
	        this.ingenicoMpos.getTransactionHistory(query)
                .then((results: TransactionHistoryResponse) => {
                    this.logMessage('history done');
                    // for(let i = 0; i < results.transactions.size(); i++) {
                    //     let item = results.transactions.get(i);
                    //     // this.logMessage('history Item:' + item.toString());
                    //     let n: TransactionHistorySummary = {
                    //         transactionId: item.getTransactionId(),
                    //         clerkId: item.getClerkId(),
                    //         transactionType: item.getTransactionType(),
                    //         amount: +item.getAmount().getTotal(),
                    //         approvedAmount: +item.getApprovedAmount()
                    //     };
                    //     this.logMessage('Adding:' + JSON.stringify(n));
                    //     this.transactionHistory.push(n);
                    //     this.logMessage('TransactionHistory:' + JSON.stringify(this.transactionHistory));
                    //     let lv = topmost().getViewById('lvHistory');
                    //     if (lv) {
                    //         lv.refresh();
                    //         console.log('lvHistory:refresh called');
                    //     } else {
                    //         console.log('lvHistory:refresh undefined');
                    //     }
                    // }
                    this.transactionHistory = new ObservableArray(results.transactions);
                    // this.logMessage('transactionHistory:' + JSON.stringify(this.transactionHistory));
                    super.notifyPropertyChange('transactionHistory', this.transactionHistory, []);
                    // let lv = topmost().getViewById('lvHistory');
                    // if (lv) {
                    //     lv.refresh();
                    //     console.log('lvHistory:refresh called');
                    // } else {
                    //     console.log('lvHistory:refresh undefined');
                    // }

                    resolve();
                })
                .catch((error:any) => {
                    this.logMessage('Error loading history...' + error);
                    reject();
                });
        });
    }

	getPermissions() : Promise<null> {
        return new Promise((resolve, reject) => {

            permissions.requestPermissions([
                    android.Manifest.permission.INTERNET,
                    android.Manifest.permission.ACCESS_NETWORK_STATE,
                    android.Manifest.permission.RECORD_AUDIO,
                    android.Manifest.permission.MODIFY_AUDIO_SETTINGS,
                    android.Manifest.permission.BLUETOOTH,
                    android.Manifest.permission.BLUETOOTH_ADMIN,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.ACCESS_FINE_LOCATION,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION ],
                "We need the following permissions.")
                .then(() => {
                    this.logMessage("Permission Granted!");
                    resolve();
                })
                .catch(() => {
                    this.logMessage("Permission Denied");
                    reject('Permission Denied');
                });
        });
    }
	logMessage(message: string) {
        let lv = topmost().getViewById('lvItems');

	    this.log.push(message);
	    if (lv) {
            lv.refresh();
            console.log('refresh called');
        } else {
            console.log('refresh undefined');
        }

		console.log('LogMessage:', message);

	}
	
	listViewLoadMoreItems(args) {
		// Expand your collection bound to the ListView with more items here!
	}

    listViewLoadMoreHistory(args) {
		// Expand your collection bound to the ListView with more items here!
	}

	test() {
	    alert('test');
    }

	voidTransaction(args) {
        let transactionId = this.cancelTransactionId;
        let clerkId = this.cancelClerkId;
        if (transactionId.trim() == '' || clerkId.trim() == '') {
            this.doAlert('Void Transaction', 'Missing value.');
            return;
        }
        let mycb = (code: number, message: string) : void => {
            this.doAlert('Update', this.ingenicoMpos.getProgressMessage(code) + ' - ' + message);
        }
        this.ingenicoMpos.voidTransaction(transactionId, clerkId, '', '', mycb)
            .then((result: CreditSaleResponse) => {
                this.doAlert('Void Transaction', result.responseCode + "::" + this.ingenicoMpos.getResponseCodeString(result.responseCode));
                if (result.responseCode == 0) {

                }
            })
            .catch((error) => {
                this.doAlert('Void Transaction', error.responseCode + "::" +
                                    this.ingenicoMpos.getResponseCodeString(error.responseCode));
            });
    }

    refundTransaction(args) {
        let transactionId = this.cancelTransactionId;
        let clerkId = this.cancelClerkId;
        let amount = this.cancelAmount;
        if (transactionId.trim() == '' || clerkId.trim() == '' || amount <= 0) {
            this.doAlert('Refund Transaction', 'Missing value.');
            return;
        }
        let mycb = (code: number, message: string) : void => {
            this.doAlert('Update', this.ingenicoMpos.getProgressMessage(code) + ' - ' + message);
        }
        let amt = {
            currency: 'USD',
            discount: 0,
            discountDescription: '',
            subtotal: 1.00,
            tax: 0.00,
            tip: 0.00,
            total: 1.00
        }
        this.ingenicoMpos.refundTransaction(transactionId, clerkId, '', '', mycb)
            .then((result: CreditSaleResponse) => {
                this.doAlert('Void Transaction', result.responseCode + "::" + this.ingenicoMpos.getResponseCodeString(result.responseCode));
                if (result.responseCode == 0) {

                }
            })
            .catch((error) => {
                this.doAlert('Void Transaction', error.responseCode + "::" +
                    this.ingenicoMpos.getResponseCodeString(error.responseCode));
            });
    }

	hideSignaturePad():void {
        this.signaturePadShowing = false;
        this.clearDrawing();
        super.notifyPropertyChange('signaturePadShowing', false, true);
        this.logMessage('turned off signature pad');
    }

	showSignaturePad():void {
		this.signaturePadShowing = true;
		this.clearDrawing();
		super.notifyPropertyChange('signaturePadShowing', true, false);
		alert('showing signature pad');
	}

    processPendingSignature() : void {
        this.ingenicoMpos.getTransactionForSignature()
            .then(result => {
                this.currentTransactionId = result;
                this.logMessage('CurrentTransactionId:' + this.currentTransactionId);
                if (this.currentTransactionId) {
                    this.showSignaturePad();
                }
            })
            .catch( error => {
                alert('There is no transactions to process.');
            });
    }

	clearDrawing(): void {
		let drawingPad = topmost().getViewById('drawingPad');    
		drawingPad.clearDrawing(); 
	}

	getSignature():void {
		let drawingPad = topmost().getViewById('drawingPad');      
		drawingPad.getDrawing().then((res) => {        
			console.log('res:', res);    
			this.signaturePadShowing = false;
			var byteArrayOutputStream:any = new java.io.ByteArrayOutputStream();
			res.compress(android.graphics.Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);

			var bytes:any[] = byteArrayOutputStream.toByteArray();
			var base64Img:any = android.util.Base64.encodeToString(bytes, android.util.Base64.DEFAULT);
			console.log(base64Img);

            if (this.currentTransactionId) {
                this.ingenicoMpos.uploadSignature(this.currentTransactionId, base64Img)
                    .then(result => {
                        this.doAlert('Signature Processed', 'Signature Uploaded.');
                        super.notifyPropertyChange('signaturePadShowing', false, true);
                    })
                    .catch(error => {
                        this.doAlert('Signature Process', 'Error processing transaction.' + error);
                    });
            } else {
                this.logMessage('there is no current transaction.');
                super.notifyPropertyChange('signaturePadShowing', false, true);
            }

		 });  
	}

	startTransaction():void {
    //     this.product = <Product>{
    //         description: 'Test Product',
    //         name: 'Test Product',
    //         price: 50.00,
    //         quantity: 1,
    //         image: null
    //     };
    //     /*
    //     currency: string;
    // total: number;
    // subtotal: number;
    // tax: number;
    // discount: number;
    // discountDescription: string;
    // tip: number;
    //      */
    //     this.amount = <Amount>{
    //         currency: 'USD',
    //         discount: 0,
    //         discountDescription: '',
    //         subtotal: 50,
    //         tax: 0.00,
    //         tip: 0.00,
    //         total: 50.00
    //     }
        /*
            amount: Amount;
    products: Product[];
    gpsLongitude: string;
    gpsLatitude: string;
    transactionGroupID: string;
         */
         // this.logMessage('Transaction Setup..' + JSON.stringify(this.saleRequest));
        // super.notifyPropertyChange('transactionStarted', true, false);


    }

    doAlert(title: string, message: string, buttonText?: string) {
	    return new Promise((resolve, reject) => {
            if (!buttonText) {
                buttonText = "Ok";
            }
            dialogs.alert({
                title: title,
                message: message,
                okButtonText: buttonText
            }).then(result => {
                resolve();
            }).catch( err => {
                reject(err);
            });
        });
    }

    testDialog(args: any) : void {
	    let a = args.object.alert;
	    console.log(JSON.stringify(args.object.alert));
	    dialogs.alert({
            title: "Test" + a,
            message: `${a}`,
            okButtonText: 'Ok'
        }).then(result => {
	        this.logMessage('Yes!!! ' + result);
        }).catch( err => {
            this.logMessage('NO!');
        });
        // dialogs.confirm({
        //     title: 'Test..This',
        //     message: `Test ${a}`,
        //     okButtonText: 'Yes',
        //     cancelButtonText: 'No',
        //     neutralButtonText: null
        // }).then(result => {
        //     this.logMessage('Yes!!' + result);
        //
        // }).catch(err => {
        //     this.logMessage('Nada..' +  err);
        //     // reject(err);
        // })
    }

    togglePadHeight():void {
	    if (this.padHeight === 400) {
	        this.padHeight = 200;
        } else {
	        this.padHeight = 400;
        }
    }

    processCreditTransaction():void {
        let saleRequest:any =  {
            amount: this.amount,
            products: [],
            gpsLongitude: '-122.084',
            gpsLatitude: '37.422',
            transactionGroupID: null
        };
        saleRequest.amount.total = +this.product.price;
        saleRequest.amount.subtotal = +this.product.price;
        saleRequest.products.push( {
            description: 'Test Product',
            name: 'Test Product',
            price: +this.product.price,
            quantity: 1,
            image: null
        });
        let mycb = (code: number, message: string) : void => {
            this.doAlert('Update', this.ingenicoMpos.getProgressMessage(code) + ' - ' + message);
        }
        this.ingenicoMpos.processCreditSale(saleRequest, mycb)
            .then((result: CreditSaleResponse)=> {
                this.logMessage('Successfully Processed Request: ' + JSON.stringify(result));
                this.currentTransactionId = result.transactionResponse.transactionGroupId;
                // this.transactionHistory.push(result.transactionResponse);
                if (this.ingenicoMpos.signaturRequired(result.transactionResponse)) {
                    alert('signature required');
                    this.logMessage('Signature Required. TransId:' + this.currentTransactionId);
                    this.showSignaturePad();
                } else {
                    this.logMessage('Transaction Complete. No signature required.');
                    this.doAlert('Sale Finished', 'ClerkDisplay:' +
                                    result.transactionResponse.clerkDisplay + "\r\n" +
                                    'TransactionID:' + result.transactionResponse.transactionGroupId);
                }

            })
            .catch((error:any) => {
                // this.transactionHistory.push(error.transactionResponse);
                this.logMessage('Error processing credit card sale..' + JSON.stringify(error));
                let transactionId = error.transactionResponse.getTransactionId();
                let clerkDisplay = error.transactionResponse.getClerkDisplay();
                let msg =
                    'Error Code:(' + error.responseCode + '): ' + this.ingenicoMpos.getResponseCodeString(error.responseCode) +
                    'ClerkDisplay:' + clerkDisplay + "\r\n" +
                    'TransactionID:' + transactionId;
                this.doAlert('Error processing credit card.', msg);
                this.logMessage(msg);
            });
    }

    processDebitTransaction():void {
	    this.logMessage('processDebitTransaction:start');
        let saleRequest:any =  {
            amount: this.amount,
            products: [],
            gpsLongitude: '-122.084',
            gpsLatitude: '37.422',
            transactionGroupID: null
        };
        saleRequest.amount.total = +this.product.price;
        saleRequest.amount.subtotal = +this.product.price;
        saleRequest.products.push( {
            description: 'Test Product',
            name: 'Test Product',
            price: +this.product.price,
            quantity: 1,
            image: null
        });
        let mycb = (code: number, message: string) : void => {
            this.doAlert('Update', this.ingenicoMpos.getProgressMessage(code) + ' - ' + message);
        }
        this.ingenicoMpos.processDebitCardSale(saleRequest, mycb)
            .then((result: CreditSaleResponse)=> {
                this.logMessage('Successfully Processed Request: ' + JSON.stringify(result));
                this.currentTransactionId = result.transactionResponse.transactionGroupId;
                if (this.ingenicoMpos.signaturRequired(result.transactionResponse)) {
                    alert('signature required');
                    this.logMessage('Signature Required. TransId:' + this.currentTransactionId);
                    this.showSignaturePad();
                } else {
                    this.logMessage('Transaction Complete. No signature required.');
                    this.doAlert('Sale Finished', 'ClerkDisplay:' +
                        result.transactionResponse.clerkDisplay + "\r\n" +
                        'TransactionID:' + result.transactionResponse.transactionGroupId);
                }

            })
            .catch((error:any) => {
                this.logMessage('Error processing credit card sale..' + JSON.stringify(error));
                let transactionId = error.transactionResponse.getTransactionId();
                let clerkDisplay = error.transactionResponse.getClerkDisplay();
                let msg =
                    'Error Code:(' + error.responseCode + '): ' + this.ingenicoMpos.getResponseCodeString(error.responseCode) +
                    'ClerkDisplay:' + clerkDisplay + "\r\n" +
                    'TransactionID:' + transactionId;
                this.doAlert('Error processing credit card.', msg);
                this.logMessage(msg);
            });
    }
}

