var IngenicoMpos = require("nativescript-ingenico-mpos").IngenicoMpos;
var ingenicoMpos = new IngenicoMpos();

describe("greet function", function() {
    it("exists", function() {
        expect(ingenicoMpos.greet).toBeDefined();
    });

    it("returns a string", function() {
        expect(ingenicoMpos.greet()).toEqual("Hello, NS");
    });
});